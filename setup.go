package cloudstack

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/coredns/caddy"
	"github.com/coredns/coredns/core/dnsserver"
	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/pkg/dnsutil"
)

func init() {
	caddy.RegisterPlugin("cloudstack", caddy.Plugin{
		ServerType: "dns",
		Action:     setup,
	})
}

func periodicFetch(h *Handler) chan bool {
	fetchChan := make(chan bool)

	if h.Options.Reload == durationOf0s {
		return fetchChan
	}

	go func() {
		ticker := time.NewTicker(h.Options.Reload)
		for {
			select {
			case <-fetchChan:
				return
			case <-ticker.C:
				h.importIPAddresses()
			}
		}
	}()
	return fetchChan
}

func setup(c *caddy.Controller) error {
	h, err := parseConfig(c)
	if err != nil {
		return plugin.Error("cloudstack", err)
	}

	fetchChan := periodicFetch(h)

	c.OnStartup(func() error {
		h.importIPAddresses()
		return nil
	})

	c.OnShutdown(func() error {
		close(fetchChan)
		return nil
	})

	dnsserver.GetConfig(c).AddPlugin(func(next plugin.Handler) plugin.Handler {
		h.Next = next
		return h
	})

	return nil
}

func parseConfig(c *caddy.Controller) (*Handler, error) {
	h := NewHandler([]string{})

	i := 0
	for c.Next() {
		// Ensure the plugin is only loaded once per server block
		if i > 0 {
			return nil, plugin.ErrOnce
		}
		i++

		// Zones
		zones := c.RemainingArgs()
		if len(zones) != 0 {
			h.Zones = zones
			for i := 0; i < len(h.Zones); i++ {
				h.Zones[i] = plugin.Host(h.Zones[i]).NormalizeExact()[0]
			}
		} else {
			h.Zones = make([]string, len(c.ServerBlockKeys))
			for i := 0; i < len(c.ServerBlockKeys); i++ {
				h.Zones[i] = plugin.Host(c.ServerBlockKeys[i]).NormalizeExact()[0]
			}
		}

		h.PrimaryZoneIndex = -1
		for idx, z := range h.Zones {
			if dnsutil.IsReverse(z) > 0 {
				continue
			}
			h.PrimaryZoneIndex = idx
			break
		}
		if h.PrimaryZoneIndex == -1 {
			return nil, errors.New("at least one non-reverse zone should be used")
		}

		for c.NextBlock() {
			switch c.Val() {
			case "api_url":
				args := c.RemainingArgs()
				if len(args) != 1 {
					return nil, c.ArgErr()
				}
				h.Options.APIUrl = args[0]
			case "api_key":
				args := c.RemainingArgs()
				if len(args) != 1 {
					return nil, c.ArgErr()
				}
				h.Options.APIKey = args[0]
			case "api_secret":
				args := c.RemainingArgs()
				if len(args) != 1 {
					return nil, c.ArgErr()
				}
				h.Options.APISecret = args[0]
			case "no_verify_ssl":
				if len(c.RemainingArgs()) != 0 {
					return nil, c.ArgErr()
				}
				h.Options.VerifySsl = false
			case "ttl":
				args := c.RemainingArgs()
				if len(args) != 1 {
					return nil, c.ArgErr()
				}
				ttl, err := strconv.Atoi(args[0])
				if err != nil {
					return h, c.Errf("ttl needs a number of second")
				}
				if ttl <= 0 || ttl > 65535 {
					return h, c.Errf("ttl provided is invalid")
				}
				h.Options.TTL = uint32(ttl)
			case "fallthrough":
				h.Fall.SetZonesFromArgs(c.RemainingArgs())
			case "reload":
				args := c.RemainingArgs()
				if len(args) != 1 {
					return nil, c.ArgErr()
				}
				reload, err := time.ParseDuration(args[0])
				if err != nil {
					return nil, fmt.Errorf("unable to parse reload duration value: '%v': %v", args[0], err)
				}
				if reload < durationOf0s {
					return h, c.Errf("invalid negative duration for reload '%s'", args[0])
				}
				h.Options.Reload = reload
			default:
				return nil, c.Errf("unknown property '%s'", c.Val())
			}
		}
	}
	if len(h.Options.APIKey) == 0 || len(h.Options.APISecret) == 0 {
		return nil, c.Err("missing api_key and/or api_secret")
	}
	return h, nil
}
