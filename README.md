# cloudstack

`cloudstack` is a [CoreDNS](https://coredns.io/) plugin for [Apache CloudStack](https://cloudstack.apache.org/).

## Name

_cloudstack_ - adds the ability to convert CloudStack server names into their public IPs.

## Description

This plugin allows to resolve names build like `*.<vm_name>.zone` into the corresponding public IP address.
It also allows PTR requests (reverse lookup).

VM Instances can be requested if they have a public IP address, with Static NAT enabled. If more than one
of these VM instances have the same hostname, then the plugin returns `NXDOMAIN` for that domain.

## Syntax

```
cloudstack [ZONES...] {
    api_url API_URL
    api_key API_KEY
    api_secret SECRET_KEY
    no_verify_ssl
    ttl SECONDS
    fallthrough [ZONES...]
    reload DURATION
}
```

With only the directive specified, the `cloudstack` plugin will default to the zone(s) specified in the server's block.
If **ZONES** is used it specifies all the zones the plugin should be authoritative for. The primary zone, used for PTR
requests, is the first non-reverse zone.

- `api_url` specifies the CloudStack API URL. It can use "http" or "https" scheme. Defaults to `http://localhost:8080/client/api`.
- `api_key` specifies the API key for the root admin user. Defaults to `coredns`. _Required_.
- `api_secret` specifies the secret key for the root admin user. _Required_.
- `no_verify_ssl` set this option to skip SSL verification. _Optional_
- `ttl` specifies the DNS TTL of the records generated (forward and reverse). Defaults to 3600 seconds (1 hour).
- `fallthrough` If zone matches and no record can be generated, pass request to the next plugin. If [**ZONES...**] is omitted,
  then fallthrough happens for all zones for which the plugin is authoritative. If specific zones are listed (for example
  `in-addr.arpa`), then only queries for those zones will be subject to fallthrough.
- `reload` specifies the duration between two consecutive CloudStack imports. A time of zero seconds disables the feature.
  Examples of valid durations: "300ms", "1.5h" or "2h45m" are valid duration with units "ns" (nanosecond), "us"
  (or "µs" for microsecond), "ms" (millisecond), "s" (second), "m" (minute), "h" (hour). Defaults to `30s`.

## Examples

Use zones from server block:

```
example.net. {
  cloudstack {
    api_url https://cloudstack.example.net:8080/client/api
    api_key jdJ...5Jp
    api_secret l84...P5k
    no_verify_ssl
    ttl 1800
    fallthrough
  }
}
```

Provide the zones where the plugin is authoritative:

```
. {
  cloudstack domain.local 192.168.5.0/24 {
    api_url https://cloudstack.example.net:8080/client/api
    api_key jdJ...5Jp
    api_secret l84...P5k
    reload 60s
  }
}
```

## Ready

This plugin reports readiness to the ready plugin. This will happen after it has synced to the
CloudStack API.

## Metrics

If monitoring is enabled (via the _prometheus_ plugin) then the following metrics are exported:

- `coredns_cloudstack_requests_total` - Counter of requests made.
- `coredns_cloudstack_entries` - Number of CloudStack Public IP addresses with static NAT.
- `coredns_cloudstack_reload_timestamp_seconds` - Timestamp of the last valid reload of CloudStack IP addresses.
- `coredns_cloudstack_api_request_duration_seconds` - Histogram of the time (in seconds) each CloudStack API request took.

## See also

- https://cloudstack.apache.org

## License

```
Copyright 2018-2022 Apalia

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this project except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
