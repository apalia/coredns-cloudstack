package cloudstack

import (
	"net"
	"strings"
	"sync"
	"time"

	cs "github.com/apache/cloudstack-go/v2/cloudstack"
	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/pkg/fall"
)

const (
	durationOf0s  = time.Duration(0)
	durationOf30s = time.Duration(30 * time.Second)
)

// Options contains plugin settings.
type Options struct {
	APIUrl    string
	APIKey    string
	APISecret string
	VerifySsl bool
	TTL       uint32
	Reload    time.Duration
}

// NewOptions creates a new Options with default values.
func NewOptions() Options {
	return Options{
		APIUrl:    "http://localhost:8080/client/api",
		VerifySsl: true,
		TTL:       3600,
		Reload:    durationOf30s,
	}
}

// Handler is the handler of this CloudStack plugin.
type Handler struct {
	sync.RWMutex

	Next             plugin.Handler
	Fall             fall.F
	Options          Options
	Zones            []string
	PrimaryZoneIndex int

	HasSynced          bool
	IPAddressesByName  map[string]net.IP
	NamesByIPAddresses map[string]string

	requestExecutor func(*Handler) (*cs.ListPublicIpAddressesResponse, error)
}

// NewHandler creates a new instance of Handler.
func NewHandler(
	zones []string,
) *Handler {
	for i := 0; i < len(zones); i++ {
		zones[i] = plugin.Host(zones[i]).NormalizeExact()[0]
	}
	return &Handler{
		Options:            NewOptions(),
		Zones:              zones,
		HasSynced:          false,
		IPAddressesByName:  make(map[string]net.IP),
		NamesByIPAddresses: make(map[string]string),
		requestExecutor:    defaultIPAddressesFetcher,
	}
}

var defaultIPAddressesFetcher = func(h *Handler) (*cs.ListPublicIpAddressesResponse, error) {
	log.Debug("Fetch CloudStack Public IP Addresses with Static NAT enabled...")

	o := h.Options
	cs := cs.NewClient(o.APIUrl, o.APIKey, o.APISecret, o.VerifySsl)
	params := cs.Address.NewListPublicIpAddressesParams()
	params.SetIsstaticnat(true)
	params.SetListall(true)
	return cs.Address.ListPublicIpAddresses(params)
}

// primaryZone will return the first non-reverse zone being handled by this plugin
func (h *Handler) primaryZone() string { return h.Zones[h.PrimaryZoneIndex] }

func (h *Handler) importIPAddresses() error {
	ipAddresses := make(map[string]net.IP)
	names := make(map[string]string)

	start := time.Now()
	resp, err := h.requestExecutor(h)
	csAPIDuration.Observe(time.Since(start).Seconds())

	if err != nil {
		log.Errorf("Error while fetching CloudStack Public IP addresses: %s", err)
		return err
	}

	csVMEntries.Set(float64(len(resp.PublicIpAddresses)))
	csReloadTime.Set(float64(time.Now().UnixNano()) / 1e9)

	for _, ipAddress := range resp.PublicIpAddresses {
		if ipAddress.Virtualmachinedisplayname != "" {
			hostname := strings.ToLower(ipAddress.Virtualmachinedisplayname)
			existing, exists := ipAddresses[hostname]
			if exists {
				log.Debugf("%v already exists for IP address %v", hostname, existing)
				delete(ipAddresses, hostname)
			} else {
				log.Debugf("Found %v -> %v", hostname, ipAddress.Ipaddress)
				ipAddresses[hostname] = net.ParseIP(ipAddress.Ipaddress).To4()
			}

			names[ipAddress.Ipaddress] = hostname
		}
	}

	h.Lock()
	h.IPAddressesByName = ipAddresses
	h.NamesByIPAddresses = names
	h.HasSynced = true
	h.Unlock()
	return nil
}
