package cloudstack

import (
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/coredns/caddy"
	"github.com/coredns/coredns/plugin/pkg/fall"
)

func TestSetupCloudStack(t *testing.T) {
	serverBlockKeys := []string{"domain.com.:8053", "dynamic.domain.com.:8053"}
	tests := []struct {
		input               string
		shouldErr           bool
		expectedErrContent  string // substring from the expected error. Empty for positive cases.
		expectedOptions     Options
		expectedZones       []string
		expectedPrimaryZone string
		expectedFallthrough fall.F
	}{
		// positive
		{
			`cloudstack {
				api_key foo
				api_secret bar
			}`,
			false, "",
			Options{"http://localhost:8080/client/api", "foo", "bar", true, 3600, 30 * time.Second},
			[]string{"domain.com.", "dynamic.domain.com."}, "domain.com.", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				reload 2m
			}`,
			false, "",
			Options{"http://localhost:8080/client/api", "foo", "bar", true, 3600, 2 * time.Minute},
			[]string{"example.net."}, "example.net.", fall.Zero,
		},
		{
			`cloudstack example.net. 10.0.1.0/24 {
				api_url https://cloudstack.example.local/api
				api_key foo
				api_secret bar
				no_verify_ssl
				ttl 1800
				fallthrough
				reload 60s
			}`,
			false, "",
			Options{"https://cloudstack.example.local/api", "foo", "bar", false, 1800, 60 * time.Second},
			[]string{"example.net.", "1.0.10.in-addr.arpa."}, "example.net.", fall.Root,
		},
		{
			`cloudstack 192.168.10.0/24 zone.local example.net {
				api_url https://cloudstack.example.local/api
				api_key foo
				api_secret bar
				no_verify_ssl
				fallthrough in-addr.arpa. test.zone.local.
			}`,
			false, "",
			Options{"https://cloudstack.example.local/api", "foo", "bar", false, 3600, 30 * time.Second},
			[]string{"10.168.192.in-addr.arpa.", "zone.local.", "example.net."}, "zone.local.",
			fall.F{Zones: []string{"in-addr.arpa.", "test.zone.local."}},
		},

		// negative
		{
			"cloudstack",
			true, "missing api_key and/or api_secret",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			"cloudstack 10.0.5.0/24",
			true, "at least one non-reverse zone should be used",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			"cloudstack example.net",
			true, "missing api_key and/or api_secret",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				reload 10
			}`,
			true, "unable to parse reload duration",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				reload -10s
			}`,
			true, "invalid negative duration for reload",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				ttl foo
			}`,
			true, "ttl needs a number of second",
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_url foo bar
			}`,
			true, "Wrong argument count or unexpected line ending", // two values for api_url
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key
				api_secret bar
			}`,
			true, "Wrong argument count or unexpected line ending", // no value for api_key
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				no_verify_ssl yo
			}`,
			true, "Wrong argument count or unexpected line ending", // value for boolean no_verify_ssl
			Options{}, []string{}, "", fall.Zero,
		},
		{
			`cloudstack example.net {
				api_key foo
				api_secret bar
				foo bar
			}`,
			true, "unknown property", // invalid property "foo"
			Options{}, []string{}, "", fall.Zero,
		},
	}

	for i, test := range tests {
		c := caddy.NewTestController("dns", test.input)
		c.ServerBlockKeys = serverBlockKeys
		h, err := parseConfig(c)

		if test.shouldErr && err == nil {
			t.Errorf("Test %d: Expected error, but did not find error for input '%s'. Error was: '%v'", i, test.input, err)
		}

		if err != nil {
			if !test.shouldErr {
				t.Errorf("Test %d: Expected no error but found one for input %s. Error was: %v", i, test.input, err)
			}

			if !strings.Contains(err.Error(), test.expectedErrContent) {
				t.Errorf("Test %d: Expected error to contain: %v, found error: %v, input: %s", i, test.expectedErrContent, err, test.input)
			}
		} else if !test.shouldErr {
			if !reflect.DeepEqual(h.Options, test.expectedOptions) {
				t.Errorf("CloudStack not correctly set for input %s. Expected: %v', actual: '%v'", test.input, test.expectedOptions, h.Options)
			}

			if !reflect.DeepEqual(h.Zones, test.expectedZones) {
				t.Errorf("CloudStack not correctly set for input %s. Expected: %v', actual: '%v'", test.input, test.expectedZones, h.Zones)
			}

			if h.primaryZone() != test.expectedPrimaryZone {
				t.Errorf("CloudStack not correctly set for input %s. Expected: %s, actual: %s", test.input, test.expectedPrimaryZone, h.primaryZone())
			}

			if !h.Fall.Equal(test.expectedFallthrough) {
				t.Errorf("CloudStack not correctly set for input %s. Expected: %s, actual: %s", test.input, test.expectedFallthrough, h.Fall)
			}
		}
	}
}
