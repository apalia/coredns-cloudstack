package cloudstack

// Ready implements the ready.Readiness interface (github.com/coredns/coredns/plugin/ready).
func (h *Handler) Ready() bool {
	h.RLock()
	defer h.RUnlock()
	return h.HasSynced
}
