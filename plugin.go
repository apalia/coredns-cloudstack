package cloudstack

import (
	"context"
	"strings"

	"github.com/coredns/coredns/plugin"
	"github.com/coredns/coredns/plugin/metrics"
	"github.com/coredns/coredns/plugin/pkg/dnsutil"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"
	"github.com/miekg/dns"
)

var log = clog.NewWithPlugin("cloudstack")

// ServeDNS implements the plugin.Handler interface.
func (h *Handler) ServeDNS(ctx context.Context, w dns.ResponseWriter, r *dns.Msg) (int, error) {
	state := request.Request{W: w, Req: r}

	name := state.Name()
	orig := state.QName()

	zone := plugin.Zones(h.Zones).Matches(name)
	if zone == "" {
		// If this doesn't match we need to fall through regardless of h.Fall
		return plugin.NextOrFailure(h.Name(), h.Next, ctx, w, r)
	}

	requestCount.WithLabelValues(metrics.WithServer(ctx), zone).Inc()

	var answers []dns.RR

	switch state.QType() {
	case dns.TypePTR:
		answers = h.lookupPtr(name, orig)
		if len(answers) == 0 {
			// If this doesn't match we need to fall through regardless of h.Fall
			return plugin.NextOrFailure(h.Name(), h.Next, ctx, w, r)
		}
	case dns.TypeA:
		answers = h.lookupA(name, zone, orig)
	default:
		answers = []dns.RR{}
	}

	m := new(dns.Msg)
	m.SetReply(r)
	m.Authoritative, m.RecursionAvailable = true, true
	m.Answer = answers
	if len(answers) == 0 {
		if h.Fall.Through(orig) {
			return plugin.NextOrFailure(h.Name(), h.Next, ctx, w, r)
		}
		m.Rcode = dns.RcodeNameError
	}

	w.WriteMsg(m)
	return dns.RcodeSuccess, nil
}

func (h *Handler) lookupPtr(name string, orig string) []dns.RR {
	ipaddress := dnsutil.ExtractAddressFromReverse(name)
	log.Debugf("CloudStack request for IP address %s", ipaddress)

	h.RLock()
	defer h.RUnlock()
	hostname, found := h.NamesByIPAddresses[ipaddress]
	if found {
		rr := new(dns.PTR)
		rr.Hdr = dns.RR_Header{
			Name:   orig,
			Rrtype: dns.TypePTR,
			Class:  dns.ClassINET,
			Ttl:    h.Options.TTL,
		}
		rr.Ptr = dnsutil.Join(hostname, h.primaryZone())
		return []dns.RR{rr}
	}
	return []dns.RR{}
}

func (h *Handler) lookupA(name string, zone string, orig string) []dns.RR {
	// name has the form "optional-subdomain.servername.zone" => get "servername"
	name, _ = dnsutil.TrimZone(name, zone)
	parts := strings.Split(name, ".")
	serverName := parts[len(parts)-1]
	log.Debugf("CloudStack request for server name %s", serverName)

	h.RLock()
	defer h.RUnlock()
	ipAddress, found := h.IPAddressesByName[serverName]
	if found {
		rr := new(dns.A)
		rr.Hdr = dns.RR_Header{
			Name:   orig,
			Rrtype: dns.TypeA,
			Class:  dns.ClassINET,
			Ttl:    3600,
		}
		rr.A = ipAddress
		return []dns.RR{rr}
	}
	return []dns.RR{}
}

// Name implements the plugin.Handler interface.
func (h *Handler) Name() string { return "cloudstack" }
