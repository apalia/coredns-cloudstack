package cloudstack

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	cs "github.com/apache/cloudstack-go/v2/cloudstack"
)

func TestImportIPAddresses(t *testing.T) {
	h := NewHandler([]string{"example.net."})
	h.requestExecutor = func(h *Handler) (*cs.ListPublicIpAddressesResponse, error) {
		resp := &cs.ListPublicIpAddressesResponse{
			Count: 5,
			PublicIpAddresses: []*cs.PublicIpAddress{
				{Ipaddress: "10.0.1.1", Virtualmachinedisplayname: "ServerA"},
				{Ipaddress: "10.0.1.2", Virtualmachinedisplayname: "ServerB"},
				{Ipaddress: "10.0.1.3", Virtualmachinedisplayname: "ServerA"},
				{Ipaddress: "10.0.1.4", Virtualmachinedisplayname: "ServerC"},
			},
		}
		return resp, nil
	}

	err := h.importIPAddresses()
	if err != nil {
		t.Errorf("Error during IP Addresses import: %v", err)
	}

	_, aFound := h.IPAddressesByName["servera"]
	if aFound {
		t.Errorf("ServerA has two IP Addresses and should not be present in map IPAddresses")
	}

	b, bFound := h.IPAddressesByName["serverb"]
	if !bFound || b.String() != "10.0.1.2" {
		t.Errorf("ServerB should be mapped to 10.0.1.2")
	}

	c, cFound := h.IPAddressesByName["serverc"]
	if !cFound || c.String() != "10.0.1.4" {
		t.Errorf("ServerC should be mapped to 10.0.1.4")
	}

	ip1, ip1Found := h.NamesByIPAddresses["10.0.1.1"]
	if !ip1Found || ip1 != "servera" {
		t.Errorf("10.0.1.1 should be mapped to servera")
	}

	ip2, ip2Found := h.NamesByIPAddresses["10.0.1.2"]
	if !ip2Found || ip2 != "serverb" {
		t.Errorf("10.0.1.2 should be mapped to serverb")
	}

	ip3, ip3Found := h.NamesByIPAddresses["10.0.1.3"]
	if !ip3Found || ip3 != "servera" {
		t.Errorf("10.0.1.3 should be mapped to servera")
	}

	ip4, ip4Found := h.NamesByIPAddresses["10.0.1.4"]
	if !ip4Found || ip4 != "serverc" {
		t.Errorf("10.0.1.4 should be mapped to serverc")
	}
}

func TestImportIPAddressesError(t *testing.T) {
	h := NewHandler([]string{"example.net."})
	h.requestExecutor = func(h *Handler) (*cs.ListPublicIpAddressesResponse, error) {
		return nil, errors.New("CloudStack error")
	}

	err := h.importIPAddresses()
	if err == nil {
		t.Errorf("It should have thrown an error.")
	}
}

func TestIPAddressesFetcher(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		isStaticNAT, hasStaticNatParameter := r.URL.Query()["isstaticnat"]
		listAll, hasListAllParameter := r.URL.Query()["listall"]
		if !hasStaticNatParameter || !hasListAllParameter || isStaticNAT[0] != "true" || listAll[0] != "true" {
			fmt.Fprintln(w, "Invalid request")
		} else {
			fmt.Fprintln(w, `{ "listpublicipaddressesresponse" : {
					"count":1,
					"publicipaddress": [
						{
							"id":"91b2869b-fb51-46cc-9a76-f8cc368c98d1",
							"ipaddress":"192.168.25.148",
							"allocated":"2018-02-06T15:31:13+0100",
							"zoneid":"3fe23bcd-2f38-44e1-b979-04f72de48c5f",
							"zonename":"AZ2dev-Apalia",
							"issourcenat":false,
							"account":"terry",
							"domainid":"168aa885-2f19-4760-9586-9ecd165b4c48",
							"domain":"perdu.com",
							"forvirtualnetwork":true,
							"vlanid":"52dceb31-355d-4529-93ca-6b1435a8bf70",
							"vlanname":"vlan://350",
							"isstaticnat":true,
							"issystem":false,
							"virtualmachineid":"31acf8eb-85ff-4447-89fe-e622af88b76c",
							"vmipaddress":"10.1.1.55",
							"virtualmachinename":"bobobob",
							"virtualmachinedisplayname":"bobobob",
							"associatednetworkid":"44a937e9-0546-4ae1-823d-dc46dbd752d7",
							"associatednetworkname":"Default Network (reserved)",
							"networkid":"4442ab33-ea2c-4035-878b-fee52a243dad",
							"state":"Allocated",
							"physicalnetworkid":"367609d1-026a-49f6-89cd-db0a36d152c9",
							"tags":[],
							"isportable":false,
							"fordisplay":true
						}
					]
				}
			}`)
		}
	}))
	defer server.Close()

	h := NewHandler([]string{"example.net."})
	h.Options.APIUrl = server.URL

	resp, err := h.requestExecutor(h)
	if err != nil {
		t.Errorf("Error in listPublicIpAddresses: %v", err)
	}

	if len(resp.PublicIpAddresses) != 1 {
		t.Errorf("Expected number of IP Addresses: %d, found %d", 1, len(resp.PublicIpAddresses))
	}
	ipaddress := resp.PublicIpAddresses[0]
	if ipaddress.Ipaddress != "192.168.25.148" {
		t.Errorf("Expected IP Addresses: 192.168.25.148, found %s", ipaddress.Ipaddress)
	}
	if ipaddress.Virtualmachinedisplayname != "bobobob" {
		t.Errorf("Expected hostname: bobobob, found %s", ipaddress.Virtualmachinedisplayname)
	}
}
