package cloudstack

import (
	"context"
	"net"
	"testing"

	"github.com/coredns/coredns/plugin/pkg/dnstest"
	"github.com/coredns/coredns/plugin/test"
	"github.com/miekg/dns"
)

func TestLookup(t *testing.T) {
	h := NewHandler([]string{"example.org.", "zone.example.com.", "10.0.1.0/24"})
	h.Next = test.ErrorHandler()
	h.IPAddressesByName = make(map[string]net.IP)
	h.IPAddressesByName["server1"] = net.ParseIP("10.0.1.1")
	h.IPAddressesByName["server2"] = net.ParseIP("10.0.1.2")
	h.NamesByIPAddresses = make(map[string]string)
	h.NamesByIPAddresses["10.0.1.1"] = "server1"
	h.NamesByIPAddresses["10.0.1.2"] = "server2"

	ctx := context.TODO()

	for _, tc := range hostsTestCases {
		m := tc.Msg()

		rec := dnstest.NewRecorder(&test.ResponseWriter{})
		_, err := h.ServeDNS(ctx, rec, m)
		if err != nil {
			t.Errorf("Expected no error, got %v", err)
			return
		}

		if err := test.SortAndCheck(rec.Msg, tc); err != nil {
			t.Error(err)
		}
	}
}

var hostsTestCases = []test.Case{
	{
		// Valid A request
		Qname: "server1.example.org.", Qtype: dns.TypeA,
		Answer: []dns.RR{
			test.A("server1.example.org. 3600	IN	A 10.0.1.1"),
		},
	},
	{
		// Valid A request with subdomain
		Qname: "foo.bar.server2.example.org.", Qtype: dns.TypeA,
		Answer: []dns.RR{
			test.A("foo.bar.server2.example.org. 3600	IN	A 10.0.1.2"),
		},
	},
	{
		// Valid A request on second zone
		Qname: "foobar.server2.zone.example.com.", Qtype: dns.TypeA,
		Answer: []dns.RR{
			test.A("foobar.server2.zone.example.com. 3600	IN	A 10.0.1.2"),
		},
	},
	{
		// Inexistant domain on valid zone
		Qname: "inexistant.example.org.", Qtype: dns.TypeA,
		Rcode: dns.RcodeNameError,
	},
	{
		// A request outside the zones
		Qname: "google.com.", Qtype: dns.TypeA,
		Rcode: dns.RcodeServerFailure,
	},
	{
		// PTR request
		Qname: "1.1.0.10.in-addr.arpa.", Qtype: dns.TypePTR,
		Answer: []dns.RR{
			test.PTR("1.1.0.10.in-addr.arpa. 3600 PTR server1.example.org."),
		},
	},
	{
		// PTR request
		Qname: "2.1.0.10.in-addr.arpa.", Qtype: dns.TypePTR,
		Answer: []dns.RR{
			test.PTR("2.1.0.10.in-addr.arpa. 3600 PTR server2.example.org."),
		},
	},
	{
		// PTR request where the server does not exist
		Qname: "3.1.0.10.in-addr.arpa.", Qtype: dns.TypePTR,
		Rcode: dns.RcodeServerFailure,
	},
	{
		// PTR request outside the zone
		Qname: "125.125.125.10.in-addr.arpa.", Qtype: dns.TypePTR,
		Rcode: dns.RcodeServerFailure,
	},
	{
		// TXT request
		Qname: "server1.example.org.", Qtype: dns.TypeTXT,
		Rcode: dns.RcodeNameError,
	},
	{
		// AAAA request
		Qname: "server1.example.org.", Qtype: dns.TypeAAAA,
		Rcode: dns.RcodeNameError,
	},
}
