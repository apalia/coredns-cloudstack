package cloudstack

import (
	"github.com/coredns/coredns/plugin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	requestCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: plugin.Namespace,
		Subsystem: "cloudstack",
		Name:      "requests_total",
		Help:      "Counter of requests made.",
	}, []string{"server", "zone"})
	csVMEntries = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: plugin.Namespace,
		Subsystem: "cloudstack",
		Name:      "entries",
		Help:      "Number of CloudStack Public IP addresses with static NAT.",
	})
	csReloadTime = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: plugin.Namespace,
		Subsystem: "cloudstack",
		Name:      "reload_timestamp_seconds",
		Help:      "Timestamp of the last valid reload of CloudStack IP addresses.",
	})
	csAPIDuration = promauto.NewHistogram(prometheus.HistogramOpts{
		Namespace: plugin.Namespace,
		Subsystem: "cloudstack",
		Name:      "api_request_duration_seconds",
		Help:      "Histogram of the time (in seconds) each CloudStack API request took.",
		Buckets:   prometheus.ExponentialBuckets(0.025, 2, 10),
	})
)
